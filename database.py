import psycopg2
from pyproj import Transformer
from math import hypot

from secrets import DB_NAME, DB_PASSWORD, DB_URL, DB_USER, SONDERINGEN_TABLE

from cpt import CPT

class Database:
    def __init__(self):
        self.connection = None
        self.cursor = None
        self.transformer = Transformer.from_crs("epsg:28992", "epsg:4326")
        self.open()

    @property
    def is_open(self):
        return self.connection is not None

    def open(self):
        if not self.is_open:
            self.connection = psycopg2.connect(f"dbname={DB_NAME} user={DB_USER} host={DB_URL} password={DB_PASSWORD}")    
          
    def execute(self, sql):
        cur = self.connection.cursor()
        cur.execute(sql)    
        return cur.fetchall()
    
    def get_cpt_closest_to(self, x_rd: float, y_rd: float, num: int=1):        
        lat, lon = self.transformer.transform(x_rd, y_rd)

        if num > 3: num = 3 

        sql = f"""WITH closest_candidates AS (
                SELECT
                    rdx, rdy, "GEF",
                    geom
                FROM
                    {SONDERINGEN_TABLE} sonderingen
                ORDER BY
                    sonderingen.geom <->
                    'SRID=4326;POINT({lat} {lon})'::geometry
                LIMIT 10
                )
                SELECT rdx, rdy, "GEF"
                FROM closest_candidates
                ORDER BY
                ST_Distance(
                    geom,
                    'SRID=4326;POINT({lat} {lon})'::geometry
                    )
                LIMIT 10;"""       
        
        cptrows = self.execute(sql)
        
        result = []
        for row in cptrows:
            dl = hypot(x_rd - row[0], y_rd - row[1])
            result.append((dl, row[2])) 
        
        result = sorted(result, key=lambda x: x[0], reverse=False)
        return [{'distance':r[0], 'cpt':CPT.from_url(r[1])} for r in result[:num]]


# if __name__=="__main__":
#     db = Database()
#     r = db.get_cpt_closest_to(139430, 445713, 3)
#     print(r)

        
