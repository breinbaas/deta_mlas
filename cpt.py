from pydantic import BaseModel
from typing import List, Dict, Any
import urllib.request

GEF_COLUMN_Z = 1
GEF_COLUMN_QC = 2
GEF_COLUMN_FS = 3
GEF_COLUMN_U = 6
GEF_COLUMN_Z_CORRECTED = 11

RF_MAX = 10.0

class CPT(BaseModel):
    x: float = 0.0
    y: float = 0.0
    z_top: float = 0.0

    z: List[float] = []
    qc: List[float] = []
    fs: List[float] = []
    u: List[float] = []
    Rf: List[float] = []

    name: str = ""
    
    filedate: str = ""
    startdate: str = ""
    filename: str = ""

    @classmethod
    def from_file(self, filename: str) -> 'CPT':
        cpt = CPT()
        cpt.filename = str(Path(filename).stem)
        cpt.read(filename)
        return cpt

    @classmethod
    def from_url(self, url: str) -> 'CPT':
        cpt = CPT()
        cpt.filename = url
        contents = urllib.request.urlopen(url).read().decode("utf-8")
        cpt.read_from_gef_stringlist(contents.split('\n'))
        return cpt
    
    @property
    def date(self) -> str:
        """Return the date of the CPT in the following order (if available) startdate, filedata, empty string (no date)

        Args:
            None

        Returns:
            str: date in format YYYYMMDD or 19000101 if invalid"""
        if self.startdate != "":
            return self.startdate
        elif self.filedate != "":
            return self.filedate
        else:
            return "19000101"

    @property
    def z_min(self) -> float:
        """
        Return the lowest point of the CPT

        Args:
            None

        Returns:
            float: deepest point in CPT
        """
        return self.z[-1]

    @property 
    def has_u(self) -> bool:
        """
        Check if the CPT has waterpressure data

        Args:
            None

        Return:
            bool: true is CPT has waterpressure readings, false otherwise
        """
        return max(self.u) > 0 or min(self.u) < 0

    def read_from_gef_stringlist(self, lines: List[str]) -> None:
        """
        Read a GEF from the indivual lines

        Args:
            lines (List[str]): list of strings

        Returns:
            None
        """
        reading_header = True
        metadata = {
            "record_seperator":"",
            "column_seperator":" ",
            "columnvoids":{}, 
            "columninfo":{}
        }
        for line in lines:
            if reading_header:
                if line.find("#EOH") >= 0:
                    reading_header = False
                else:
                    self._parse_header_line(line, metadata)
            else:
                self._parse_data_line(line, metadata)

        self._calculate()

    def read(self, filename: str) -> None:
        extension = str(Path(filename).suffix.lower())

        if extension == ".gef":
            self._read_gef(filename)
        else:
            raise NotImplementedError(f"Unknown and unhandled file extension {extension}")
        
    def _read_gef(self, filename: str) -> None:
        """
        Read a GEF file

        Args:
            filename (str): the name of the file to be read

        Returns:
            None
        """
        with open(filename, "r", encoding="ascii", errors="ignore") as f:
            lines = [l.replace("\x00", "") for l in f.readlines()]

        self.read_from_gef_stringlist(lines)

        

    def _calculate(self) -> None:
        """
        Calculate other parameters from the qc and fs values

        Args:
            None

        Returns:
            None"""
        for qc, fs in zip(self.qc, self.fs):
            if qc == 0.0:
                self.Rf.append(RF_MAX)
            else:
                self.Rf.append(round(fs / qc * 100., 3))

    def _parse_header_line(self, line: str, metadata: Dict[str, Any]) -> None:
        try:
            args = line.split("=")
            keyword, argline = args[0], args[1]
        except Exception as e:
            raise ValueError(f"Error reading headerline '{line}' -> error {e}")

        keyword = keyword.strip().replace("#", "")
        argline = argline.strip()
        args = argline.split(",")

        if keyword == "RECORDSEPARATOR":
            metadata["record_seperator"] = args[0]
        elif keyword == "COLUMNSEPARATOR":
            metadata["column_seperator"] = args[0]
        elif keyword == "COLUMNINFO":
            try:
                column = int(args[0])
                dtype = int(args[3].strip())
                if dtype == GEF_COLUMN_Z_CORRECTED:
                    dtype = GEF_COLUMN_Z # use corrected depth instead of depth                
                metadata["columninfo"][dtype] = column - 1
            except Exception as e:
                raise ValueError(f"Error reading columninfo '{line}' -> error {e}")
        elif keyword == "XYID":
            try:
                self.x = round(float(args[1].strip()), 2)
                self.y = round(float(args[2].strip()), 2)
            except Exception as e:
                raise ValueError(f"Error reading xyid '{line}' -> error {e}")
        elif keyword == "ZID":
            try:
                self.z_top = float(args[1].strip())
            except Exception as e:
                raise ValueError(f"Error reading zid '{line}' -> error {e}")     
        elif keyword == "COLUMNVOID":
            try:
                col = int(args[0].strip())
                metadata["columnvoids"][col - 1] = float(args[1].strip())
            except Exception as e:
                raise ValueError(f"Error reading columnvoid '{line}' -> error {e}")  
        elif keyword == "TESTID":
            self.name = args[0].strip() 
        elif keyword == "FILEDATE":
            try:
                yyyy = int(args[0].strip())
                mm = int(args[1].strip())
                dd = int(args[2].strip())   

                if yyyy < 1900 or yyyy > 2100 or mm < 1 or mm > 12 or dd < 1 or dd > 31:
                    raise ValueError(f"Invalid date {yyyy}-{mm}-{dd}")            

                self.filedate = f"{yyyy}{mm:02}{dd:02}"
            except:                
                self.filedate = ""
        elif keyword == "STARTDATE":
            try:
                yyyy = int(args[0].strip())
                mm = int(args[1].strip())
                dd = int(args[2].strip())
                self.startdate = f"{yyyy}{mm:02}{dd:02}"
                if yyyy < 1900 or yyyy > 2100 or mm < 1 or mm > 12 or dd < 1 or dd > 31:
                    raise ValueError(f"Invalid date {yyyy}-{mm}-{dd}")
            except:                
                self.startdate = ""
        
    def _parse_data_line(self, line: str, metadata: Dict[str, Any]) -> None:
        try:
            if len(line.strip()) == 0: return
            args = line.replace(metadata["record_seperator"], '').strip().split(metadata["column_seperator"])
            fargs = [float(arg.strip()) for arg in args if len(arg.strip()) > 0]
            
            # skip lines that have a columnvoid
            for col_index, voidvalue in metadata["columnvoids"].items():
                if fargs[col_index] == voidvalue:
                    return  

            zcolumn = metadata["columninfo"][GEF_COLUMN_Z]
            qccolumn = metadata["columninfo"][GEF_COLUMN_QC]
            fscolumn = metadata["columninfo"][GEF_COLUMN_FS]

            ucolumn = -1
            if GEF_COLUMN_U in metadata["columninfo"].keys():
                ucolumn = metadata["columninfo"][GEF_COLUMN_U]

            dz = self.z_top - abs(fargs[zcolumn])            
            self.z.append(round(dz,3))

            qc = fargs[qccolumn]
            if qc <= 0:
                qc = 1e-3
            self.qc.append(round(qc,3))
            fs = fargs[fscolumn]
            if fs <= 0:
                fs = 1e-6
            self.fs.append(round(fs,6))

            if ucolumn > -1:
                self.u.append(round(fargs[ucolumn],6))
            else:
                self.u.append(0.0)

        except Exception as e:
            raise ValueError(f"Error reading dataline '{line}' -> error {e}")