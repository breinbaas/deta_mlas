from fastapi import FastAPI
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

from database import Database

app = FastAPI()
db = Database()

@app.get("/")
async def root():    
    return "Hello World!"

@app.get("/cpts/")
async def get_cpts(x: int = 0, y: int = 0, num: int = 1):
    cptdata = db.get_cpt_closest_to(x, y, num)

    jsondata = jsonable_encoder(cptdata)
    return JSONResponse(content=jsondata)    


